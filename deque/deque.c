#include <stdlib.h>
#include <assert.h>
#include <stddef.h>

#include "deque.h"

#define SIDES (2)

struct deque {
  struct deque *next[SIDES];
  int data;
};

// initialize deque
Deque *dequeCreate(void) {
  Deque *d;

  d = malloc(offsetof(struct deque, data));

  if (d) {
    d->next[DEQUE_FRONT] = d->next[DEQUE_BACK] = d;
  }

  return d;
}

// push new data onto side of deque d
void dequePush(Deque *d, int side, int data) {
  struct deque *node;

  assert(side == DEQUE_FRONT || side == DEQUE_BACK);

  node = malloc(sizeof(struct deque));
  assert(node);

  node->next[side] = d->next[side];
  node->next[!side] = d;
  node->data = data;

  d->next[side] = node;
  node->next[side]->next[!side] = node;
}

// pop and returns first value on side of deque d
// returns DEQUE_EMPTY if deque is empty
int dequePop(Deque *d, int side) {
  struct deque *node;
  int data;

  assert(side == DEQUE_FRONT || side == DEQUE_BACK);

  node = d->next[side];

  if (node == d) {
    return DEQUE_EMPTY;
  }

  d->next[side] = node->next[side];
  node->next[side]->next[!side] = d;

  data = node->data;

  free(node);

  return data;
}

// return 1 if deque contains no elements, 0 otherwise
int dequeIsEmpty(const Deque *d) {
  return d->next[DEQUE_FRONT] == d;
}

// free space used by a deque
void dequeDestroy(Deque *d) {
  while (!dequeIsEmpty(d)) {
    dequePop(d, DEQUE_FRONT);
  }

  free(d);
}