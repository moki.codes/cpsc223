#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define STACK_EMPTY (0)

struct e {
  struct e *next;
  int data;
};

typedef struct e *Stack;

void stackPush(Stack *s, int data) {
  struct e *node;
  node = malloc(sizeof(struct e));

  assert(node);

  node->next = *s;
  node->data = data;

  *s = node;
}

int isStackEmpty(const Stack *s) {
    return !(*s);
}
  
int stackPop(Stack *s) {
  int data;
  struct e *node;
  
  assert(!isStackEmpty(s));

  data = (*s)->data;

  node = *s;
  *s = node->next;

  return data;
}

void stackDisplay(Stack *s) {
  struct e *node = *s;

  while (node) {
    printf("%d", node->data);
    node = node->next;
  }

  putchar('\n');
  
}

void stackDisplayReverse(struct e *cursor) {
  if (cursor) {
    stackDisplayReverse(cursor->next);

    printf("%d", cursor->data);
  }
}

int main() {
  int i;
  Stack s;

  s = STACK_EMPTY;

  for(i = 0; i < 5; i++) {
    printf("push %d\n", i);
    stackPush(&s, i);
    stackDisplay(&s);
  }

  while (!isStackEmpty(&s)) {
    printf("pop gets %d\n", stackPop(&s));
    stackDisplay(&s);
  }
  
  for(i = 0; i < 5; i++) {
    printf("push %d\n", i);
    stackPush(&s, i);
    stackDisplay(&s);
  }

  printf("Display Reversed:\n");
  stackDisplayReverse(s);
  putchar('\n');

  return 0;
}