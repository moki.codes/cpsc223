typedef struct ids *Ids;

#define MIN_ID (0)
#define MAX_ID (999999999)

// build a list of ids out of an unsorted array of n good ids
// returns 0 on allocation failure
Ids IdsCreate(int, int[]);

// destroy an id list
void IdsDestroy(Ids);

// check an id against the list
// returns nonzero if id is in the list
int IdsContains(Ids, int);
