#include <stdio.h>
#include "ids.h"

int main() {
  printf("Hello \n");
  int uids[5];
  int i;
  for (i = 0; i < 5; i++) {
    uids[i] = i;
  }

  Ids _ids = IdsCreate(i, uids);

  printf("Does list contain 11: %d\n", IdsContains(_ids, 11));
  printf("Does list contain 3: %d\n", IdsContains(_ids, 3));
  printf("Does list contain 2: %d\n", IdsContains(_ids, 2));
  printf("Does list contain 7: %d\n", IdsContains(_ids, 7));
}