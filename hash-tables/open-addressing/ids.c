#include <stdlib.h>
#include <assert.h>

#include "ids.h"

#define OVERHEAD (1.1)
#define NULL_ID (-1)

struct ids {
  int size;
  int _ids[1];
};

// build a list of ids out of an unsorted array of n good ids
// returns 0 on allocation failure
Ids IdsCreate(int n, int usIds[]) {
  Ids list;
  int size;
  int i;
  int probe;

  size = (int) (n * OVERHEAD + 1);

  list = malloc(sizeof(*list) + sizeof(int) * (size - 1));

  if (!list) {
    return 0;
  }

  list->size = size;

  for (i = 0; i < size; i++) {
    list->_ids[i] = NULL_ID;
  }

  for (i = 0; i < n; i++) {
    assert(usIds[i] >= MIN_ID);
    assert(usIds[i] <= MAX_ID);

    for (
      probe = usIds[i] % list->size;
      list->_ids[probe] != NULL_ID;
      probe = (probe + 1) % list->size
    );
    
    assert(list->_ids[probe] == NULL_ID);

    list->_ids[probe] = usIds[i];
  }

  return list;
}

// destroy an id list
void IdsDestroy(Ids list) {
  free(list);
}

// check an id against the list
// returns nonzero if id is in the list
int IdsContains(Ids list, int id) {
  int probe;

  for (
    probe = id % list->size;
    list->_ids[probe] != NULL_ID;
    probe = (probe + 1) % list->size) {
      if (list->_ids[probe]) {
        return 1;
      }
  }

  return 0;
}
