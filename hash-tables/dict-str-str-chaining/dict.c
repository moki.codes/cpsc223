#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "dict.h"

#define INITIAL_SIZE (1024)
#define GROWTH_FACTOR (2)
#define MAX_LOAD_FACTOR (1)

#define MULTIPLIER (97)

struct node {
  struct node *next;
  char *key;
  char *val;
};

struct dict {
  int size;
  int n;
  struct node **table;
};

char *strdup(const char *s);

static Dict _DictCreate(int size) {
  Dict d;
  int i;

  d = malloc(sizeof(*d));

  assert(d);

  d->size = size;
  d->n = 0;
  d->table = malloc(sizeof(struct node *) * d->size);

  assert(d->table);

  for (i = 0; i < d->size; d->table[i++] = 0); // possible buggy behavior

  for (i = 0; i < d->size; i++) {
    assert(d->table[i] == 0);
  }

  return d;
}

// mult hashing fn
static unsigned long hash_fn(const char *s) {
  unsigned const char *us;
  unsigned long h;

  h = 0;

  for (us = (unsigned const char *) s; *us; us++) {
    h = h * MULTIPLIER + *us;
  }

  return h;
}

static void grow(Dict d) {
  Dict d2;
  struct dict swap;
  int i;
  struct node *nd;

  d2 = _DictCreate(d->size * GROWTH_FACTOR);

  for (i = 0; i < d->size; i++) {
    for (nd = d->table[i]; nd != 0; nd = nd->next) {
      DictInsert(d2, nd->key, nd->val);
    }
  }

  swap = *d;
  *d = *d2;
  *d2 = swap;

  DictDestroy(d2);
}

// instantiate
Dict DictCreate(void) {
  return _DictCreate(INITIAL_SIZE);
}

// destroy
void DictDestroy(Dict d) {
  int i;

  struct node *nd;
  struct node *next;

  for (i = 0; i < d->size; i++) {
    for (nd = d->table[i]; nd != 0; nd = next) {
      next = nd->next;

      free(nd->key);
      free(nd->val);
      free(nd);
    }
  }

  free(d->table);
  free(d);
}

// insert a new key-val pair into dict
void DictInsert(Dict d, const char *key, const char *val) {
  struct node *nd;

  unsigned long h;

  assert(key);
  assert(val);

  nd = malloc(sizeof(*nd));

  assert(nd);

  nd->key = strdup(key);
  nd->val = strdup(val);

  h = hash_fn(key) % d->size;

  nd->next = d->table[h];
  d->table[h] = nd;
  
  d->n++;

  if (d->n >= d->size * MAX_LOAD_FACTOR) {
    grow(d);
  }
}

// return the most recently added val associated with a key
// 0 on failure
const char *DictSearch(Dict d, const char *key) {
  struct node *nd;

  for (nd = d->table[hash_fn(key) % d->size]; nd != 0; nd = nd->next) {
    if (!strcmp(key, nd->key)) {
      return nd->val;
    }
  }

  return 0;
}

// delete the most recently inserted val associated with a key
// no effect if no such value found
void DictDelete(Dict d, const char *key) {
  struct node **pn;
  struct node *nd;

  for (pn = &(d->table[hash_fn(key) % d->size]); *pn != 0; pn = &((*pn)->next)) {
    if (!strcmp((*pn)->key, key)) {
      nd = *pn;
      *pn = nd->next;

      free(nd->key);
      free(nd->val);
      free(nd);

      return ;
    }
  }
}