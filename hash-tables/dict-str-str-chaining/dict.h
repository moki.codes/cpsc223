typedef struct dict *Dict;

// instantiate
Dict DictCreate(void);

// destroy
void DictDestroy(Dict);

// insert a new key-val pair into dict
void DictInsert(Dict, const char *, const char *);

// return the most recently added val associated with a key
// 0 on failure
const char *DictSearch(Dict, const char *);

// delete the most recently inserted val associated with a key
// no effect if no such value found
void DictDelete(Dict, const char *);