#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

struct node {
  int data;
  struct node *next;
};

struct queue {
  struct node *head;
  struct node *tail;
};

typedef struct queue *q;

struct queue *Queue(void) {
  struct queue *q;

  q = malloc(sizeof(struct queue));

  q->head = q->tail = NULL;

  return q;
}

void enqueue(struct queue *q, int data) {
  struct node *newnode;
  newnode = malloc(sizeof(struct node));

  assert(newnode);

  newnode->data = data;
  newnode->next = NULL;

  if (!(q->head)) {
    q->head = newnode;
  } else {
    q->tail->next = newnode;
  }

  q->tail = newnode;
}

int isQueueEmpty(const struct queue *q) {
  return !(q->head);
}

int dequeue(struct queue *q) {
  int data;
  struct node *oldhead;

  assert(!isQueueEmpty(q));

  data = q->head->data;

  oldhead = q->head;
  q->head = oldhead->next;
  
  free(oldhead);

  return data;
}

void queueDisplay(struct queue *q) {
  struct node *cursor = q->head;
  
  while(cursor) {
    printf("%d", cursor->data);
    cursor = cursor->next;
  }

  putchar('\n');
}

void queueDestroy(struct queue *q) {
  while(!isQueueEmpty(q)) {
    dequeue(q);
  }

  free(q);
}

int main() {
  struct queue *q = Queue();
  int i;
  
  for(i = 0; i < 5; i++) {
    printf("enq %d\n", i);
    enqueue(q, i);
    queueDisplay(q);
  }

  while(!isQueueEmpty(q)) {
    printf("deq gets %d\n", dequeue(q));
    queueDisplay(q);
  }

  queueDestroy(q);

  return 0;
}