#include <stdlib.h>
#include "sequence.h"

struct sequence {
  int next;
  int step;
};

Sequence seq_create_step(int initial, int step) {
  Sequence s;

  s = malloc(sizeof(*s));
  if (!s) {
    return 0;
  }

  s->next = initial;
  s->step = step;

  return s;
}

Sequence seq_create(int initial) {
  return seq_create_step(initial, 1);
}

void seq_destroy(Sequence s) {
  free(s);
}

int seq_next(Sequence s) {
  int data;

  data = s->next;
  s->next += s->step;

  return data;
}