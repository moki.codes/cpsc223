#include <stdio.h>
#include "sequence.h"

void sequence_print(Sequence s, int limit);

int main(void) {
  Sequence s;
  Sequence s2;

  puts("Stepping by 1:");

  s = seq_create(0);
  sequence_print(s, 5);
  seq_destroy(s);

  puts("Now stepping by 3:");

  s2 = seq_create_step(1, 3);
  sequence_print(s2, 20);
  seq_destroy(s2);

  return 0;
}

void sequence_print(Sequence s, int limit) {
  int i;

  for (i = seq_next(s); i < limit; i = seq_next(s)) {
    printf("%d\n", i);
  }
}