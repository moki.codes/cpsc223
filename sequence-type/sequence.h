typedef struct sequence *Sequence;

Sequence seq_create(int);

Sequence seq_create_step(int, int);

void seq_destroy(Sequence);

int seq_next(Sequence);