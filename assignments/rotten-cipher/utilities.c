#include <stdio.h>
#include <stdlib.h>
#include "utilities.h"

// magic numbers
extern const int MAXLINES = 10;
extern const int MAXLENGTH = 512;

void encode(char *c, int *shift) {
  if (*c >= 'A' && *c <= 'Z') {
    *c = 'A' + (((*c - 'A') + *shift) % 26);
    *shift = (*shift + 5) % 26;
  } else if (*c >= 'a' && *c <= 'z') {
    *c = 'a' + (((*c - 'a') + *shift) % 26);
    *shift = (*shift + 3) % 26;
  }
}

int readlines(char *sb, char ss[MAXLINES][MAXLENGTH]) {
  int *shift = malloc(sizeof(int *));
  *shift = 17;
  int i = 0;
  void *r;
  while ((r = fgets(sb, MAXLENGTH, stdin)) != NULL && i < MAXLINES) {
    // copy string to 2d array
    char *sbp = sb;
    int j = 0;
    while(*sbp && j < MAXLENGTH) {
      encode(sbp, shift);
      ss[i][j++] = *sbp++;
    }
    ss[i][j-1] = '\0';
    i++;
  }

  return i;
}