#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utilities.h"
#include "caesar-cipher.h"
#include <assert.h>

int main() {
  // string buffer allocation
  char *sb = malloc(sizeof(char *) * MAXLENGTH);

  // declaration of 2d array
  char ss[MAXLINES][MAXLENGTH];
  
  int i;
  i = readlines(sb, ss);
  
  // display inputed lines
  int j = 0;

  printf("echo:\n");
  while(j < i) {
    printf("%s\n", ss[j]);
    ++j;
  }

  // tests
  assert(strcmp(ss[0], "\"Jpnr, yptsw!\" woihj Ccd, uorhycucygw.") == 0);
  assert(strcmp(ss[1], "\"Zud'xa fztfv akxu fa znndp!\" fvjiihj ctt umgnmuky, vnjdtjiwzp.") == 0);

  return 0;
}