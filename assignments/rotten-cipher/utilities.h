#include <stdio.h>

// magic numbers
extern const int MAXLINES;
extern const int MAXLENGTH;

int readlines(char *, char[MAXLINES][MAXLENGTH]);

