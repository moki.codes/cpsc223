typedef struct deque Deque;

#define DEQUE_FRONT (0)
#define DEQUE_BACK (1)

#define DEQUE_EMPTY (-1)

// initialize deque
Deque *dequeCreate(void);

// push new data onto side of deque d
void dequePush(Deque *d, int side, int data);

// pop and returns first value on side of deque d
// returns DEQUE_EMPTY if deque is empty
int dequePop(Deque *d, int side);

// return 1 if deque contains no elements, 0 otherwise
int dequeIsEmpty(const Deque *d);

// free space used by a deque
void dequeDestroy(Deque *d);