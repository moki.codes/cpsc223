#include <stdlib.h>
#include <assert.h>

#include "deque.h"

#define INITIAL_SIZE (8)

struct deque {
  size_t base;
  size_t length;
  size_t size;
  int *data;
};

// internal initialization method
static Deque *_dequeCreate(size_t size) {
  struct deque *d;

  d = malloc(sizeof(struct deque));
  assert(d);

  d->base = 0;
  d->length = 0;
  d->size = size;

  d->data = malloc(sizeof(int) * d->size);
  assert(d->data);

  return d;
}

// initialize deque
Deque *dequeCreate(void) {
  return _dequeCreate(INITIAL_SIZE);
}

// push new data onto side of deque d
void dequePush(Deque *d, int side, int data) {
  struct deque *deq;
  int *olddata;

  if (d->length == d->size) {
    deq = _dequeCreate(d->size * 2);

    while (!dequeIsEmpty(d)) {
      dequePush(deq, DEQUE_BACK, dequePop(d, DEQUE_FRONT));
    }

    olddata = d->data;
    *d = *deq;

    free(olddata);
    free(deq);
  }

  if (side == DEQUE_FRONT) {
    if (d->base == 0) {
      d->base = d->size - 1;
    } else {
      d->base--;
    }

    d->length++;
    d->data[d->base] = data;
  } else {
    d->data[(d->base + d->length++) % d->size] = data;
  }
}

// pop and returns first value on side of deque d
// returns DEQUE_EMPTY if deque is empty
int dequePop(Deque *d, int side) {
  int data;

  if (dequeIsEmpty(d)) {
    return DEQUE_EMPTY;
  }

  if (side == DEQUE_FRONT) {
    data = d->data[d->base];

    d->base = (d->base+1) % d->size;
    d->length--;

    return data;
  } else {
    return d->data[(d->base + --d->length) % d->size];
  }
}

// return 1 if deque contains no elements, 0 otherwise
int dequeIsEmpty(const Deque *d) {
  return !d->length;
}

// free space used by a deque
void dequeDestroy(Deque *d) {
  free(d->data);
  free(d);
}