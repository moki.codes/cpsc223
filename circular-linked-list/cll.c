#include <stdlib.h>

#define RIGHT (0)
#define LEFT (1)

struct node {
  struct node *next[2];
};

typedef struct node *Node;

Node listCreate(void) {
  Node node;

  node = malloc(sizeof(*node));

  if (node) {
    node->next[LEFT] = node->next[RIGHT] = node;
  }

  return node;
}

void listRemove(Node node) {
  node->next[RIGHT]->next[LEFT] = node->next[LEFT];
  node->next[LEFT]->next[RIGHT] = node->next[RIGHT];
}


// insert node into list in direction side from head
void listInsert(Node head, int side, Node node) {
  node->next[side] = head->next[side];
  node->next[!side] = head;

  node->next[side]->next[!side] = node;
  node->next[!side]->next[side] = node;
}


// splice a list starting at right after left
// right becomes lefts right neighbor
// rights left neighbor becomes left neighbor of lefts old right neighbor
void listSplit(Node left, Node right) {
  right->next[RIGHT]->next[LEFT] = left->next[RIGHT];
  left->next[RIGHT]->next[LEFT] = right->next[LEFT];

  left->next[RIGHT] = right;
  right->next[LEFT] = left;
}

void listDestroy(Node node) {
  Node target;
  Node next;

  for (target = node->next[RIGHT]; target != node; target = next) {
    next = target->next[RIGHT];
    free(target);
  }

  free(node);
}
